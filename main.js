//обработчик событий на загрузку страницы
window.onload = function() {
    alert('Hello! The page is successfully loaded.');
}


// Координаты на странице, задание 1
let buttonShownOnScroll = document.getElementById('buttonShownOnScroll');
// обработчик событий на скролл
window.onscroll = function () {
    if (document.documentElement.scrollTop > buttonShownOnScroll.getBoundingClientRect().top) {
        buttonShownOnScroll.style.opacity = '1';
    } else {
        buttonShownOnScroll.style.opacity = '0';
    }
};
//Вверх
let timeOut;
function goUp() {
    let top = Math.max(document.documentElement.scrollTop);
    if (top > 0) {
        window.scrollBy(0, -100000);
        timeOut = setTimeout('goUp()', 1000);
    } else clearTimeout(timeOut);
}


// Координаты на странице, TABS-блок, задание 2
document.getElementById('defaultOpen').click();

function openTab(evt, tabName) {
    let i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName('tab__content');
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = 'none';
    }

    tablinks = document.getElementsByClassName('tab__link');
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', '');
    }

    document.getElementById(tabName).style.display = 'block';
    evt.currentTarget.className += ' active';
}


// Dropdown закрывается при нажатии на другие кнопки меню
// не работает с querySelectorAll
function myDropdown() {
    document.getElementById('myDropdown').classList.add('show');
    if (document.getElementById('myDropdown').classList.contains('show')) {
        document.getElementById('tabBlockLink').onclick = function () {
            document.getElementById('myDropdown').classList.remove('show');
        }
        document.getElementById('visitorsLink').onclick = function () {
            document.getElementById('myDropdown').classList.remove('show');
        }
        document.getElementById('contentLink').onclick = function () {
            document.getElementById('myDropdown').classList.remove('show');
        }
        document.getElementById('spaceLink').onclick = function () {
            document.getElementById('myDropdown').classList.remove('show');
        }
    }
}

// убегающий блок
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
let runner = document.querySelector('.runner');
function running() {
    runner.style.top=getRandomInt(0, 300)+"px";
    runner.style.left=getRandomInt(0, 600)+"px";
}

function youWin() {
    alert('You win!');
}


// Попытка подчеркнуть активный пункт меню на одной странице
window.addEventListener('scroll', () => {
    let scrollDistance = window.scrollY;

    document.querySelectorAll('.section').forEach((el, i) => {
        if (el.offsetTop - document.querySelector('.nav').clientHeight <= scrollDistance) {
            document.querySelectorAll('.nav ul li a').forEach((el) => {
                if (el.classList.contains('current')) {
                    el.classList.remove('current');
                }
            });
            document.querySelectorAll('.nav li')[i].querySelector('a').classList.add('current');
        }
    });
});

